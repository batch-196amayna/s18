console.log("Hello World");
//--------------------------------------------------------------------------------
function printAddMath(num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of 5 and 15: ");
	console.log(sum);
};
printAddMath(5, 15);
//--------------------------------------------------------------------------------
function printSubMath(num1, num2){
	let difference = num1 - num2;
	console.log("Displayed difference of 20 and 5: ");
	console.log(difference);
};
printSubMath(20, 5);
//--------------------------------------------------------------------------------
function printMulMath(num1, num2){
	console.log("The product of 50 and 10: ");
	return num1 * num2;
};
let product = printMulMath(50, 10);
console.log(product);
//--------------------------------------------------------------------------------
function printQuoMath(num1, num2){
	console.log("The quotient of 50 and 10: ");
	return (num1 / num2);
};
let quotient = printQuoMath(50, 10);
console.log(quotient);
//--------------------------------------------------------------------------------
function printAreaCircle(num){
	console.log("The area of a circle with 15 radius: ");
	return ((3.14)*(num**2));
};
let circleArea = printAreaCircle(15);
console.log(circleArea);
//--------------------------------------------------------------------------------
function printAverage(num1, num2, num3, num4){
	console.log("The Average of 20, 40, 60 and 80: ");
	return (num1 + num2 + num3 +num4)/4;
};
let averageVar = printAverage(20, 40, 60, 80);
console.log(averageVar);
//--------------------------------------------------------------------------------
function printPercentage(num1, num2){
	console.log("Is 38/50 is a passing score: ");
	let percent = ((num1 / num2) * 100);
	return passing = percent >= 75;
};
let percentage = printPercentage(38, 50);
console.log(percentage);